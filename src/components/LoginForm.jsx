import React, { useState } from "react";

const LoginForm = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const handleFormSubmit = (e) => {
    e.preventDefault();
    console.log(email, password);
  };
  return (
    <section className="w-full h-full">
      <div className="py-4 text-center">
        <h1 className="font-bold text-lg lg:text-xl capitalize">
          {" "}
          user authentication
        </h1>
      </div>
      {/* Form */}
      <div className="w-1/5 mx-auto space-y-2">
        <form
          className="flex justify-center items-center gap-4 flex-col "
          onSubmit={handleFormSubmit}
        >
          <input
            type="email"
            className="border border-black/50 p-2 placeholder:capitalize placeholder:font-medium rounded-lg w-full outline-none peer"
            id="email"
            placeholder="email address"
            onChange={(e) => setEmail(e.target.value)}
            value={email}
            required
          />
          <p class=" invisible peer-invalid:visible text-pink-600 text-sm">
            Please provide a valid email address.
          </p>
          <input
            type="password"
            className="border border-black/50 p-2 placeholder:capitalize placeholder:font-medium rounded-lg w-full outline-none"
            id="password"
            placeholder="enter password"
            onChange={(e) => setPassword(e.target.value)}
            value={password}
            required
          />
        

          <input
            type="submit"
            className="bg-emerald-500 px-4 py-2 capitalize rounded-lg"
            value="Login"
          />
        </form>
        <a href="#"  className="underline capitalize pt-2 text-blue-600 ">
          forget password?
        </a>
      </div>
      <div>
        <h2 className="capitalize"></h2>
      </div>
    </section>
  );
};

export default LoginForm;
